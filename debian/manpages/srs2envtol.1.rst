==========
srs2envtol
==========

------------------------------------------
Translate a SRS encoded email address back
------------------------------------------

:Datum: 2014-03-17
:Version: 1.0
:Manual section: 1

SYNOPSIS
========
*srs2envtol* `srsemail`


DESCRIPTION
===========
Reverses `srsemail` back to there normal email address. It does the opposite of ``envfrom2srs``.


.. warning ::
        Use only if absolutely necessary.  It is *very* inefficient and a security risk.

OPTIONS
=======
The program uses `/etc/mail/pysrs.cfg`, if available::

  [srs]
  secret = 'shhhh!'
  maxage = 8
  hashlength = 8
  separator = '='

SEE ALSO
========
``envfrom2srs``\(1)

