Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: pysrs
Upstream-Contact: Stuart D. Gathman <stuart@bmsi.com>
Source: https://github.com/sdgathman/pysrs.git

Files: *
Copyright: 2004 Shevek
           2004-2010 Business Management Systems
License: PSF-2.4

Files: debian/*
Copyright: 2013-2022 Sandro Knauß <hefee@debian.org>
License: PSF-2.4

Files: pysrs.m4
       pysrsprog.m4
Copyright: 2004 Alain Knaff
           1988, 1993 The Regents of the University of California
           2004-2009 Business Management Systems, Inc
License: sendmail

License: PSF-2.4
 PSF LICENSE AGREEMENT FOR PYTHON 2.4
 ------------------------------------
 .
 1. This LICENSE AGREEMENT is between the Python Software Foundation
 ("PSF"), and the Individual or Organization ("Licensee") accessing and
 otherwise using Python 2.4 software in source or binary form and its
 associated documentation.
 .
 2. Subject to the terms and conditions of this License Agreement, PSF
 hereby grants Licensee a nonexclusive, royalty-free, world-wide
 license to reproduce, analyze, test, perform and/or display publicly,
 prepare derivative works, distribute, and otherwise use Python 2.4
 alone or in any derivative version, provided, however, that PSF's
 License Agreement and PSF's notice of copyright, i.e., "Copyright (c)
 2001, 2002, 2003, 2004 Python Software Foundation; All Rights Reserved"
 are retained in Python 2.4 alone or in any derivative version prepared
 by Licensee.
 .
 3. In the event Licensee prepares a derivative work that is based on
 or incorporates Python 2.4 or any part thereof, and wants to make
 the derivative work available to others as provided herein, then
 Licensee hereby agrees to include in any such work a brief summary of
 the changes made to Python 2.4.
 .
 4. PSF is making Python 2.4 available to Licensee on an "AS IS"
 basis.  PSF MAKES NO REPRESENTATIONS OR WARRANTIES, EXPRESS OR
 IMPLIED.  BY WAY OF EXAMPLE, BUT NOT LIMITATION, PSF MAKES NO AND
 DISCLAIMS ANY REPRESENTATION OR WARRANTY OF MERCHANTABILITY OR FITNESS
 FOR ANY PARTICULAR PURPOSE OR THAT THE USE OF PYTHON 2.4 WILL NOT
 INFRINGE ANY THIRD PARTY RIGHTS.
 .
 5. PSF SHALL NOT BE LIABLE TO LICENSEE OR ANY OTHER USERS OF PYTHON
 2.4 FOR ANY INCIDENTAL, SPECIAL, OR CONSEQUENTIAL DAMAGES OR LOSS AS
 A RESULT OF MODIFYING, DISTRIBUTING, OR OTHERWISE USING PYTHON 2.4,
 OR ANY DERIVATIVE THEREOF, EVEN IF ADVISED OF THE POSSIBILITY THEREOF.
 .
 6. This License Agreement will automatically terminate upon a material
 breach of its terms and conditions.
 .
 7. Nothing in this License Agreement shall be deemed to create any
 relationship of agency, partnership, or joint venture between PSF and
 Licensee.  This License Agreement does not grant permission to use PSF
 trademarks or trade name in a trademark sense to endorse or promote
 products or services of Licensee, or any third party.
 .
 8. By copying, installing or otherwise using Python 2.4, Licensee
 agrees to be bound by the terms and conditions of this License

License: sendmail
 SENDMAIL LICENSE
 .
 The following license terms and conditions apply, unless a different
 license is obtained from Sendmail, Inc., 6425 Christie Ave, Fourth Floor,
 Emeryville, CA 94608, USA, or by electronic mail at license@sendmail.com.
 .
 License Terms:
 .
 Use, Modification and Redistribution (including distribution of any
 modified or derived work) in source and binary forms is permitted only if
 each of the following conditions is met:
 .
 1. Redistributions qualify as "freeware" or "Open Source Software" under
    one of the following terms:
 .
    (a) Redistributions are made at no charge beyond the reasonable cost of
        materials and delivery.
 .
    (b) Redistributions are accompanied by a copy of the Source Code or by an
        irrevocable offer to provide a copy of the Source Code for up to three
        years at the cost of materials and delivery.  Such redistributions
        must allow further use, modification, and redistribution of the Source
        Code under substantially the same terms as this license.  For the
        purposes of redistribution "Source Code" means the complete compilable
        and linkable source code of sendmail including all modifications.
 .
 2. Redistributions of source code must retain the copyright notices as they
    appear in each source code file, these license terms, and the
    disclaimer/limitation of liability set forth as paragraph 6 below.
 .
 3. Redistributions in binary form must reproduce the Copyright Notice,
    these license terms, and the disclaimer/limitation of liability set
    forth as paragraph 6 below, in the documentation and/or other materials
    provided with the distribution.  For the purposes of binary distribution
    the "Copyright Notice" refers to the following language:
    "Copyright (c) 1998-2004 Sendmail, Inc.  All rights reserved."
 .
 4. Neither the name of Sendmail, Inc. nor the University of California nor
    the names of their contributors may be used to endorse or promote
    products derived from this software without specific prior written
    permission.  The name "sendmail" is a trademark of Sendmail, Inc.
 .
 5. All redistributions must comply with the conditions imposed by the
    University of California on certain embedded code, whose copyright
    notice and conditions for redistribution are as follows:
 .
    (a) Copyright (c) 1988, 1993 The Regents of the University of
        California.  All rights reserved.
 .
    (b) Redistribution and use in source and binary forms, with or without
        modification, are permitted provided that the following conditions
        are met:
 .
       (i)   Redistributions of source code must retain the above copyright
             notice, this list of conditions and the following disclaimer.
 .
       (ii)  Redistributions in binary form must reproduce the above
             copyright notice, this list of conditions and the following
             disclaimer in the documentation and/or other materials provided
             with the distribution.
 .
       (iii) Neither the name of the University nor the names of its
             contributors may be used to endorse or promote products derived
             from this software without specific prior written permission.
 .
 6. Disclaimer/Limitation of Liability: THIS SOFTWARE IS PROVIDED BY
    SENDMAIL, INC. AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED
    WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
    MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN
    NO EVENT SHALL SENDMAIL, INC., THE REGENTS OF THE UNIVERSITY OF
    CALIFORNIA OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
    INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
    NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
    USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
    ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
    (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
    THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 .
 $Revision: 1.1 $, Last updated $Date: 2010/03/18 02:19:03 $
